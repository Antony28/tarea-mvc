﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class Door : MonoBehaviour
{
    private Animator anim;


    void Start()
    {
         anim = GetComponent<Animator>();
    }
    public void OpenDoor()
    {
        Debug.Log("Open Door");
        anim.enabled = !anim.enabled;
    }
}
