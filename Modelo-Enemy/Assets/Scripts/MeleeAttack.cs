﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttack : MonoBehaviour
{
    EnemyLife enemyComp;

    public bool canAttack;

    void Start()
    {
        canAttack = false;
        enemyComp = GameObject.FindGameObjectWithTag("Enemy").GetComponent<EnemyLife>();
    }

    void Update()
    {
        if(canAttack == true)
        {
            if(Input.GetKeyDown(KeyCode.X))
            {
                Debug.Log("Attack");
                enemyComp.TakeDamage();
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            Debug.Log("Can Attack");
            canAttack = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            Debug.Log("Can't Attack");
            canAttack = false;
        }
    }
}
