﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyModelo : MonoBehaviour
{
    EnemyVista eVista;

    public int initialLife;
    public int currentLife;

    public int playerDmg;

    void Start()
    {
        eVista = gameObject.GetComponent<EnemyVista>();
    }
    void Update()
    {
        if (currentLife <= 0)
        {
            eVista.Die();
        }
    }
}
