﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyVista : MonoBehaviour
{
    EnemyModelo eModelo;
    PlayerVista pVista;


    void Start()
    {
        eModelo = gameObject.GetComponent<EnemyModelo>();
        pVista = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerVista>();
    }
    public void TakeDamage()
    {
        eModelo.currentLife -= eModelo.playerDmg;
    }
    public void Die()
    {
        Debug.Log("Enemy Die");
        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            pVista.TakeDamage();
            pVista.SetLifeText();
        }
    }
}
