﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControlador : MonoBehaviour
{
    EnemyModelo eModelo;

    void Start()
    {
        eModelo = gameObject.GetComponent<EnemyModelo>();
        eModelo.currentLife = eModelo.initialLife;
    }
}
