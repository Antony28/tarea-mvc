﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseModeloo : MonoBehaviour
{
    public bool pause;
    public GameObject pauseMenu;

    void Start()
    {
        pause = false;
        pauseMenu.SetActive(false);
    }
}
