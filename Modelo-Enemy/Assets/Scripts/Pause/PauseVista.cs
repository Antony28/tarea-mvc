﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseVista : MonoBehaviour
{
    PauseModeloo pauseModelo;

    void Start()
    {
        pauseModelo = gameObject.GetComponent<PauseModeloo>();
    }
    public void Pause()
    {
        pauseModelo.pause = !pauseModelo.pause;
        Time.timeScale = (pauseModelo.pause) ? 0 : 1;
        pauseModelo.pauseMenu.SetActive(pauseModelo.pause);
    }
}
