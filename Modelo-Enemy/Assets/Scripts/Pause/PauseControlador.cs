﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseControlador : MonoBehaviour
{
    PauseVista pauseVista;

    void Start()
    {
        pauseVista = gameObject.GetComponent<PauseVista>();
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pauseVista.Pause();
        }
    }
}
